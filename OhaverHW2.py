import matplotlib.pyplot as pl
x=[1.,2.,3.,4.]
y=[1.,7.,3.2,9.7]
dy=[.1,.2,.15,.3]
pl.errorbar(x,y,dy,0,'D')
pl.xlim(0,5)
pl.ylim(0,10.5)
