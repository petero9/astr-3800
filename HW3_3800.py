import pylab as p

def Planck(wavelength,T,R,d):
    
    """
    Plot of flux at earth by wavelength.
    Form:
    Plot(wavelength (nm), T (K), R (m), d (pc))
    """
    
    h=6.626068e-34
    k=1.3806503e-23
    c=299792458.
    
    wavelength /= 1e9
    #R *= 1e6
    d /= 3.24e-29
    
    return 2*h*c**2/(wavelength**5*(p.exp(h*c/wavelength/k/T)-1))*4*p.pi*R**2/d**2

waves=range(100,1500,5)
p.plot(waves, [Planck(wave, 5777.,6.96e9,4.85e-6) for wave in waves], color='y',label="Sun")
p.plot(waves, [Planck(wave, 8000.,6.96e9,7.275e-6) for wave in waves], color='g',label="8000K, 1.5AU")
p.plot(waves, [Planck(wave, 4000.,6.96e9,2.425e-6) for wave in waves], color='r',label="4000K, .5AU")
p.xlabel(r'$\lambda$ (cm)')
p.ylabel(r'$\phi_{\lambda}(T)$')
p.legend(loc=1)
p.title("$\phi$ vs $\lambda$")
p.show()
